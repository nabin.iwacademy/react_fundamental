// import React from "react";
// import ReactDOM from "react-dom";
// import CommentDetail from "./CommentDetail";
// import ApprovalCard from "./ApprovalCard";
// import faker from "faker";
// const App = () => {
//   return (
//     <div className="ui container comments">
//       <ApprovalCard>Are you sure you wanna do this?</ApprovalCard>
//       <ApprovalCard>
//         <CommentDetail
//           author="Enrique"
//           timeAgo="Today 3:00"
//           content="I like this blog post"
//           avatar={faker.image.avatar()}
//         />
//       </ApprovalCard>
//       <ApprovalCard>
//         <CommentDetail
//           author="Leonardo"
//           timeAgo="Today 2:00"
//           content="This blog is awesome."
//           avatar={faker.image.avatar()}
//         />
//       </ApprovalCard>
//       <ApprovalCard>
//         <CommentDetail
//           author="harry"
//           timeAgo="Today 4:00"
//           content="I like java"
//           avatar={faker.image.avatar()}
//         />
//       </ApprovalCard>
//     </div>
//   );
// };

// ReactDOM.render(<App />, document.querySelector("#root"));

import React from "react";
import ReactDOM from "react-dom";
import SeasonDisplay from "./SeasonDisplay";
import Spinner from "./Spinner";

class App extends React.Component {
  // constructor(props) {
  //   super(props);
  //   // this is the only one time we do direct assignment
  //   this.state = {
  //     lat: null,
  //     errorMessage: "",
  //   };
  // }
  state = { lat: null, errorMessage: "" };

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      // we called setState
      (position) => this.setState({ lat: position.coords.latitude }),
      // we shouldn't do this
      // this.state.lat = position.coords.latitude;
      (err) => this.setState({ errorMessage: err.message })
    );
  }
  // componentDidMount() {
  //   console.log("my component is render to this screen.");
  // }

  // componentDidUpdate() {
  //   console.log("my componenet is update");
  // }
  renderContent() {
    if (this.state.errorMessage && !this.state.lat) {
      return <div>error:{this.state.errorMessage}</div>;
    }
    if (this.state.lat && !this.state.errorMessage) {
      return <SeasonDisplay lat={this.state.lat} />;
    }
    return <Spinner message="Please accept location request" />;
  }
  // React says we have to defined render
  render() {
    return <div className="boarder red">{this.renderContent()}</div>;
  }
}
ReactDOM.render(<App />, document.querySelector("#root"));
